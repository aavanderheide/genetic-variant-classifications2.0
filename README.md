# README #

Author: Aaron van der Heide  
Version: 2.0  
Date: 22-11-2020  
Student Bioinformatics at Hanzehogeschool Groningen.    

### Repository info ###

This repository contains the log and all its files. To see what is going on here please open the DefiniteLogAndReport folder and from there open the Aaron_vd_Heide_Th9_CompleteLog.Rmd or .pdf.
The names of the files introduce themselves.

### Additional Info ###
* This is the second version of the research. The things that changed are listed down below.
* - The structure of the EDA repo has become more structured.
* - Figure 1 from the log was a histogram and changed to a boxplot.
* - All the code in the log is shown now. All hashes and echo=F have been removed.
* - All the relevant information from the results(classifier).csv are stored in a .txt file and these are shown instead of the whole classifier output.
* - The lineair regression in Log figure 19 and 20 is removed. A clear ROC-curve is now visible.
* - There are more figures(1 and 5) added to the Report that originate from the EDA with the corresponding explanation.
* - The screenshot of the clusters is removed and replaced by a heatmap made with ggplot which is originates from the EDA.
* - the phrase: “The log is very easygoing so everyone should be possible to reproduce this research. ” is removed from the report.
