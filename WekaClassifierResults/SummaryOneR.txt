OneR
Correctly Classified Instances       57407               88.0638 %
Incorrectly Classified Instances      7781               11.9362 %

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0.805    0.050    0.804      0.805    0.805      0.754    0.877     0.687     benign
                 0.856    0.088    0.561      0.856    0.678      0.644    0.884     0.497     probably_damaging
                 0.996    0.005    0.997      0.996    0.997      0.991    0.996     0.996     malignant
                 0.000    0.000    ?          0.000    ?          ?        0.500     0.060     possibly_damaging
                 0.000    0.000    ?          0.000    ?          ?        0.500     0.000     unknown
Weighted Avg.    0.881    0.023    ?          0.881    ?          ?        0.929     0.818     

=== Confusion Matrix ===

     a     b     c     d     e   <-- classified as
 10726  2514    89     0     0 |     a = benign
  1068  6447    16     0     0 |     b = probably_damaging
   104    54 40234     0     0 |     c = malignant
  1434  2485    13     0     0 |     d = possibly_damaging
     4     0     0     0     0 |     e = unknown
